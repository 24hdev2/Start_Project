// Datatable
let tbody = document.querySelector(".table-body");
const itemsPerPage = document.querySelector(".items-per-page");
const pageNumberButton = document.querySelector(".pagination");
const paginationButtons = document.querySelectorAll(".pagination-button");
const btnPrev = document.querySelector(".btn-prev");
const btnNext = document.querySelector(".btn-next");
const btnPrev_First = document.querySelector(".btn-prev-first");
const btnNext_Last = document.querySelector(".btn-next-last");
const infor = document.querySelector(".infor-text");
const inputSearch = document.querySelector(".input-search");
const infoFilter = document.querySelector(".info-filter");
const sorts = document.querySelectorAll(".sort");
let sortBy = "name";
let order = "asc";
let toggle = false;
let activeSortId = "name";
let currentPage = 1;
let limit = itemsPerPage.value;
let listCurrent = [];
let list = [];
const dataTable = "../data/data.json";

// ferch data
function getData(callback) {
  fetch(dataTable)
    .then(function (response) {
      return response.json();
    })
    .then(function (response) {
      list = response;
      listCurrent = list;
      return response;
    })
    .then(callback);
}
function formatWithCommas(number) {
  return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
function convertDateFormat(inputDate) {
  const parts = inputDate.split("/");
  const formattedDate = parts[2] + "/" + parts[1] + "/" + parts[0];
  return formattedDate;
}

function renderdata(employee, index) {
  let bg = "";
  let boxshadow = "";
  index % 2 === 0 ? (bg = "old") : (bg = "even");
  return `<tr class="${bg}">
        <td>${employee.name}</td>
        <td>${employee.position}</td>
        <td>${employee.office}</td>
        <td>${employee.age}</td>
        <td>${convertDateFormat(employee.startDate)}</td>
        <td>$${formatWithCommas(employee.salary).toLocaleString("en-US")}</td>
    </tr>`;
}

function showItemReload() {
  sort();
  loadItem();
  renderPaginationButtons();
  infor.innerHTML = changeDataTableInfo();
}

function loadItem() {
  let htmls = "";
  let begin = limit * (currentPage - 1);
  let end = limit * currentPage - 1;
  listCurrent.forEach((item, index) => {
    if (index >= begin && index <= end) {
      htmls += renderdata(item, index);
    }
  });
  tbody.innerHTML = htmls;
}

function renderPaginationButtons() {
  let htmlBtns = "";
  const sumPage = Math.ceil(listCurrent.length / limit);
  if (sumPage > 5) {
    if (currentPage <= 2) {
      for (let i = 1; i <= 3; i++) {
        htmlBtns += buttonHTML(i);
      }
      htmlBtns += `...<button class="btn btn-hover" onclick="handleClick(event.target,${
        sumPage - 1
      })">${
        sumPage - 1
      }</button><button class="btn btn-hover" onclick="handleClick(event.target,${sumPage})">${sumPage}</button>`;
    } else if (currentPage >= sumPage - 1) {
      htmlBtns += `<button class="btn btn-hover" onclick="handleClick(event.target,1)">1</button>...`;
      for (let i = sumPage - 2; i <= sumPage; i++) {
        htmlBtns += buttonHTML(i);
      }
    } else {
      htmlBtns += `<button class="btn btn-hover" onclick="handleClick(event.target,1)">1</button>...`;
      for (let i = currentPage - 1; i <= currentPage + 1; i++) {
        htmlBtns += buttonHTML(i);
      }
      htmlBtns += `...<button class="btn btn-hover" onclick="handleClick(event.target,${sumPage})">${sumPage}</button>`;
    }
  } else {
    for (let i = 1; i <= sumPage; i++) {
      htmlBtns += buttonHTML(i);
    }
  }
  pageNumberButton.innerHTML = htmlBtns;
}

function buttonHTML(i) {
  console.log(currentPage);
  if (i === currentPage) {
    return `<button class="btn btn-selected" onclick="handleClick(event.target,${i})" style="border: 1px solid #ccc; background-color: #f8f8f8;">${i}</button>`;
  } else {
    return `<button class="btn btn-hover" onclick="handleClick(event.target,${i})">${i}</button>`;
  }
}

function changePage(i) {
  currentPage = i;
  loadItem();
  renderPaginationButtons();
}

function changeStatusButton(button) {
  const btnActive = document.querySelector(".btn.btn-selected");
  if (btnActive) {
    btnActive.classList.remove("btn-selected");
    btnActive.classList.add("btn-hover");
  }
  button.classList.remove("btn-hover");
  button.classList.add("btn-selected");
}

function changeDataTableInfo() {
  let begin = limit * (currentPage - 1);
  let end = limit * currentPage - 1;
  if (listCurrent.length > 0) {
    return `
        Showing ${begin + 1} to ${
      end + 1 < listCurrent.length ? end + 1 : listCurrent.length
    } of ${listCurrent.length} entries
    `;
  } else {
    return `
        Showing 0 to ${
          end + 1 < listCurrent.length ? end + 1 : listCurrent.length
        } of ${listCurrent.length} entries
    `;
  }
}

function disableButtonPrev() {
  if (currentPage < 2) {
    btnPrev.disabled = true;
    btnPrev.classList.remove("btn-hover");
  } else {
    btnPrev.disabled = false;
    btnPrev.classList.add("btn-hover");
  }
}
function disableButtonPrev_First() {
  if (currentPage == 1) {
    btnPrev_First.disabled = true;
    btnPrev_First.classList.remove("btn-hover");
  } else {
    btnPrev_First.disabled = false;
    btnPrev_First.classList.add("btn-hover");
  }
}

function disableButtonNext() {
  const number = Math.ceil(listCurrent.length / limit);
  console.log(number);
  if (currentPage > number - 1) {
    btnNext.disabled = true;
    btnNext.classList.remove("btn-hover");
  } else {
    btnNext.disabled = false;
    btnNext.classList.add("btn-hover");
  }
}

function disableButtonNext_Last() {
  const number = Math.ceil(listCurrent.length / limit);
  if (currentPage == number) {
    btnNext_Last.disabled = true;
    btnNext_Last.classList.remove("btn-hover");
  } else {
    btnNext_Last.disabled = false;
    btnNext_Last.classList.add("btn-hover");
  }
}

// Search function
function search(valueinput) {
  return list.filter((item) => {
    for (let x of Object.values(item)) {
      if (typeof x === "string") {
        if (x.toLowerCase().includes(valueinput.toLowerCase())) return item;
      } else {
        if (x == valueinput) return item;
      }
    }
  });
}

// Sort function
function sort() {
  const listSort = listCurrent.sort((a, b) => {
    if (a[sortBy] < b[sortBy]) {
      return order === "asc" ? -1 : 1;
    }
    if (a[sortBy] > b[sortBy]) {
      return order === "asc" ? 1 : -1;
    }
    return 0;
  });
  listCurrent = listSort;
}

// event click
function handleClick(button, i) {
  changePage(i);
  changeStatusButton(button);
  disableButtonPrev();
  disableButtonNext();
  disableButtonPrev_First();
  disableButtonNext_Last();
  infor.innerHTML = changeDataTableInfo();
}

// event select
itemsPerPage.addEventListener("change", (e) => {
  limit = e.target.value;
  currentPage = 1;
  showItemReload();
  disableButtonPrev();
  disableButtonNext();
  disableButtonPrev_First();
  disableButtonNext_Last();
});

// input
inputSearch.addEventListener("input", () => {
  if (inputSearch.value == "") {
    infoFilter.innerHTML = "";
    listCurrent = list;
  } else {
    infoFilter.innerHTML = `(filtered from ${list.length} total entries)`;
    listCurrent = search(inputSearch.value.trim());
    currentPage = 1;
  }
  showItemReload();
});

// const clearIcon = document.createElement("i");
// clearIcon.classList.add("fas", "fa-times", "clear-icon");
// const formSearch = document.querySelector(".form-search");

// inputSearch.addEventListener("input", () => {
//   if (inputSearch.value == "") {
//     infoFilter.innerHTML = "";
//     listCurrent = list;
//     clearIcon.style.display = "none";
//   } else {
//     infoFilter.innerHTML = `(filtered from ${list.length} total entries)`;
//     listCurrent = search(inputSearch.value.trim());
//     currentPage = 1;
//     clearIcon.style.display = "block";
//   }
//   showItemReload();
// });

// // Add the clear icon to the formSearch element
// inputSearch.insertAdjacentElement("afterend", clearIcon);

// // Add a click event listener to the clear icon
// clearIcon.addEventListener("click", () => {
//   inputSearch.value = "";
//   clearIcon.style.display = "none";
//   infoFilter.innerHTML = "";
//   listCurrent = list;
//   currentPage = 1;
//   showItemReload();
// });


// prev/next
paginationButtons.forEach((paginationButton) => {
  paginationButton.addEventListener("click", (e) => {
    if (e.target.id === "prev") {
      currentPage--;
    } else if (e.target.id === "next") {
      currentPage++;
    } else if (e.target.id === "prev_first") {
      currentPage = 1;
    } else {
      currentPage = Math.ceil(listCurrent.length / limit);
    }
    disableButtonNext(e.target);
    disableButtonPrev(e.target);
    disableButtonPrev_First(e.target);
    disableButtonNext_Last(e.target);
    changePage(currentPage);
    infor.innerHTML = changeDataTableInfo();
  });
});

// sort
sorts.forEach((sort) => {
  sort.addEventListener("click", (e) => {
    if (activeSortId !== e.target.id) toggle = false;
    activeSortId = e.target.id;
    sortBy = e.target.id;
    const sortingASC = document.querySelector(".sort.sorting_asc");
    const sortingDESC = document.querySelector(".sort.sorting_desc");
    if (sortingASC) sortingASC.classList.remove("sorting_asc");
    if (sortingDESC) sortingDESC.classList.remove("sorting_desc");
    if (toggle === false) {
      order = "asc";
      e.target.classList.add("sorting_asc");
      e.target.classList.remove("sorting_desc");
    } else {
      order = "desc";
      e.target.classList.remove("sorting_asc");
      e.target.classList.add("sorting_desc");
    }
    toggle = !toggle;
    currentPage = 1;
    showItemReload();
  });
});

function main() {
  try {
    getData(showItemReload);
    disableButtonPrev();
    disableButtonPrev_First();
    clearIcon.style.display = "none";
  } catch (error) {
    alert("Loading failed");
  } finally {
  }
}

main();
