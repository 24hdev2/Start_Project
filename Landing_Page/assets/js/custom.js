const html = document.documentElement
const headerNavItems = document.querySelectorAll('.header__nav-item');
const typedTextSpan = document.querySelector(".typed-text");
const cursorSpan = document.querySelector(".cursor");
const textArray = ["founders.","designers.","developers.",];
const typingDelay = 50;
const erasingDelay = 50;
const newTextDelay = 2000;
let textArrayIndex = 0;
let charIndex = 0;
const iconMenu = document.querySelector('.icon-menu')
const iconClose = document.querySelector('.icon-close')
const headerDropDown = document.querySelectorAll('.event-down')
const iconArrowDownList = document.querySelectorAll('.arrow-down')
const modal = document.querySelector('.modal')
const subnavList = document.querySelectorAll('.subnav-item .hidden')
const sections = document.querySelectorAll('section');
const textTyping = document.querySelector('.text-typing')
const inputCheck = document.querySelector("input[type=checkbox]")
const pricingAmount = document.querySelector('.pricing-amount')
const countElements = document.querySelectorAll('.count');
const inputs = document.querySelectorAll('.form-input input')
const messInputs = document.querySelectorAll('.mess-input')
const label = document.querySelectorAll('.form-input label')
const form = document.querySelector('.about form')
let height = 100
let animationCountDone = false


//SHOW NAVIGATION
headerNavItems.forEach(function(navItem) {
    const headerSubnav = navItem.querySelector('.header__subnav');

    navItem.addEventListener('mouseenter', function() {
        headerSubnav.style.display = 'flex';
        headerSubnav.style.opacity = '1';
        headerSubnav.style.animation = 'subnav .2s linear';
    });

    navItem.addEventListener('mouseleave', function() {
        headerSubnav.style.animation = 'subnav-out .2s linear';

        headerSubnav.addEventListener('animationend', function() {
            headerSubnav.style.display = 'none';
            headerSubnav.style.opacity = '0';
        }, { once: true });
    });
});

var navLinks = document.querySelectorAll('.header__nav-link');
navLinks.forEach(function (link) {
    link.addEventListener('click', function () {
        var subnav = this.nextElementSibling;
        if (subnav.style.display === 'flex' || subnav.style.display === '') {
            subnav.style.display = 'none';
        } else {
            subnav.style.display = 'flex';
            setTimeout(function () {
                subnav.style.display = 'none';
            }, 100);
        }
    });
});

//TYPING TEXT
function type() {
  if (charIndex < textArray[textArrayIndex].length) {
    if(!cursorSpan.classList.contains("typing")) cursorSpan.classList.add("typing");
    typedTextSpan.textContent += textArray[textArrayIndex].charAt(charIndex);
    charIndex++;
    setTimeout(type, typingDelay);
  } 
  else {
    cursorSpan.classList.remove("typing");
  	setTimeout(erase, newTextDelay);
  }
}

function erase() {
	if (charIndex > 0) {
    if(!cursorSpan.classList.contains("typing")) cursorSpan.classList.add("typing");
    typedTextSpan.textContent = textArray[textArrayIndex].substring(0, charIndex-1);
    charIndex--;
    setTimeout(erase, erasingDelay);
  } 
  else {
    cursorSpan.classList.remove("typing");
    textArrayIndex++;
    if(textArrayIndex>=textArray.length) textArrayIndex=0;
    setTimeout(type, typingDelay + 10);
  }
}

document.addEventListener("DOMContentLoaded", function() {
  if(textArray.length) setTimeout(type, newTextDelay + 100);
});

// Onclick MENU
iconMenu.addEventListener('click', () => {
    modal.classList.add('show');
    document.body.style.overflowY = 'hidden';
    document.documentElement.style.overflow = 'hidden';
    modal.style.marginRight = '0';
});

iconClose.addEventListener('click', () => {
    modal.classList.remove('show');
    document.body.style.overflowY = 'scroll';
    modal.style.marginRight = '0';
});

headerDropDown.forEach((item, index) => {
    item.addEventListener('click', (event) => {
        const subnavShow = document.querySelector('.subnav-item .show');
        if (subnavShow !== null) {
            if (subnavShow !== subnavList[index]) {
                subnavShow.classList.remove('show');
            }
        }
        subnavList[index].classList.toggle('show');
        iconArrowDownList[index].classList.toggle('fa-chevron-down');
        iconArrowDownList[index].classList.toggle('fa-chevron-up');
        if (iconArrowDownList[index].classList.contains('fa-chevron-up')) {
            iconArrowDownList[index].style.fontSize = '0.7em';
        } else {
            iconArrowDownList[index].style.fontSize = '';
        }
        for (let i = 0; i < iconArrowDownList.length; i++) {
            if (i !== index && iconArrowDownList[i].classList.contains('fa-chevron-up')) {
                iconArrowDownList[i].classList.remove('fa-chevron-up');
                iconArrowDownList[i].classList.add('fa-chevron-down');
            }
        }

        event.stopPropagation();
    });
});

document.addEventListener('click', (event) => {
    const subnavShow = document.querySelector('.subnav-item .show');
    if (subnavShow !== null) {
        subnavShow.classList.remove('show');
        for (let i = 0; i < iconArrowDownList.length; i++) {
            if (iconArrowDownList[i].classList.contains('fa-chevron-up')) {
                iconArrowDownList[i].classList.remove('fa-chevron-up');
                iconArrowDownList[i].classList.add('fa-chevron-down');
            }
        }
    }
});



//COUNT
function countUp(upto, num, countElement) {
    if (upto <= num) {
      countElement.innerHTML = upto;
      setTimeout(() => {
        countUp(++upto, num, countElement);
      }, 20);
    } 
  }

function countDown(downto, num, countElement) {
    if (downto >= num) {
        countElement.innerHTML = downto;
        setTimeout(() => {
            countDown(--downto, num, countElement);
        }, 20);
      } 
}

// animation scroll element
window.addEventListener('scroll', function() {
    sections.forEach((section, index) => {
        if(index != 0) {
            if(`${section.id}` === 'stats'){
                    const top = section.getBoundingClientRect().top
                    const bottom = section.getBoundingClientRect().bottom
                    if(top < this.window.innerHeight && bottom >= 0) {
                        if(animationCountDone === false){
                            countElements.forEach((countElement) => {
                                countUp(0, countElement.innerHTML, countElement);
                            });
                            animationCountDone = true
                        }
                    }
            }
            const animations = document.querySelectorAll(`#${section.id} .animation`)
            animations.forEach(animation => {
                const top = animation.getBoundingClientRect().top
                const bottom = animation.getBoundingClientRect().bottom
                if(top < this.window.innerHeight && bottom >= 0) {
                    animation.classList.add(`${animation.dataset.animation}`)
                }
            })
        }
    })
})


// Slider
const cardImg = document.querySelector('.card-img img');
const slider = document.getElementById('my-slider')
const slides = slider.querySelector('.slides')
const prevButton = slider.querySelector('.prev')
const nextButton = slider.querySelector('.next')
const images = Array.from(slides.children)
const first = slides.firstElementChild
const last = slides.lastElementChild


let index = 1
let offset = 0
let dragStart = null

function cloneImage (image, refImage) {
  const clone = image.cloneNode()
  slides.insertBefore(clone, refImage)
  return clone
}

function disableNativeDragging (element) {
  element.draggable = false
}

function withTransitionSuspended (callback) {
  let scheduled = null

  return () => {
    if (scheduled) {
      window.cancelAnimationFrame(scheduled)
    }

    scheduled = window.requestAnimationFrame(() => {
      slides.style.transition = 'none'
      callback()

      scheduled = window.requestAnimationFrame(() => {
        slides.style.transition = ''
        scheduled = null
      })
    })
  }
}

function toggleDisabled (disabled) {
  prevButton.disabled = nextButton.disabled = disabled
}

function getOffset () {
  return images.slice(0, index).reduce((width, image) =>
    width + image.clientWidth, 0)
}

function translateSlides (deltaX = 0) {
  slides.style.transform = `translateX(${-offset + deltaX}px)`
}
function shiftSlides(newIndex = index) {
    index = newIndex;
    offset = getOffset();
    translateSlides();
    const newImageSrc = index === 1 ? '../assets/img/photo-1.jpg' : '../assets/img/photo-26.jpg';
    cardImg.src = newImageSrc;
    cardImg.classList.add('animation-slider');
    setTimeout(() => {
      cardImg.classList.remove('animation-slider');
    }, 500);
}
  

function fakeInfinity () {
  switch (index) {
    case 0:
      return shiftSlides(images.length - 2)
    case images.length - 1:
      return shiftSlides(1)
    default:
  }
}

function startDragging (event) {
  slides.style.transition = 'none'
  dragStart = event.clientX
  toggleDisabled(true)
}

function doDragging (event) {
  if (dragStart === null) {
    return
  }

  translateSlides(event.clientX - dragStart)
}

function stopDragging (event) {
  if (dragStart === null) {
    return
  }

  slides.style.transition = ''
  shiftSlides(index + (event.clientX < dragStart ? 1 : -1))
  dragStart = null
}

images.push(cloneImage(first, null))
images.unshift(cloneImage(last, first))
;[slides, ...images].forEach(disableNativeDragging)
prevButton.addEventListener('click', () => shiftSlides(index - 1))
nextButton.addEventListener('click', () => shiftSlides(index + 1))
slides.addEventListener('mousedown', startDragging)
slides.addEventListener('mousemove', doDragging)
slides.addEventListener('mouseleave', stopDragging)
slides.addEventListener('mouseup', stopDragging)
slides.addEventListener('transitionstart', () => toggleDisabled(true))
slides.addEventListener('transitionend', () => toggleDisabled(false))
slides.addEventListener('transitionend', withTransitionSuspended(fakeInfinity))
window.addEventListener('resize', withTransitionSuspended(shiftSlides))
window.dispatchEvent(new Event('resize'))



//Checkbox price
inputCheck.addEventListener('change', () => {
    if(inputCheck.checked){
        countUp(pricingAmount.innerHTML, 49, pricingAmount);
    } else {
        countDown(49, 29, pricingAmount);
    }
})


// Validate input
const messEmpty = "Please enter this field."
const messEmail = "Invalid email."
const messPassword = "Please enter a password with length > 8."
let isInputValid = false

const isEmptyInput = (value) => {
    if(value) return true
    else return false
}

const checkEmail = (email) => {
    let regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
    if(email.match(regex)){
        return true
    }
    return false
}

const checkMinLength = (str, minlength) => {
    if(str.length < minlength) return false
    return true 
}

const validateInput = (input, index) => {
    if(!isEmptyInput(input.value)){
        messInputs[index].innerHTML = messEmpty
    } 
    else if(!checkEmail(input.value) && index === 1){
        messInputs[index].innerHTML = messEmail
    }
    else if(!checkMinLength(input.value, 8) && index === 2){
        messInputs[index].innerHTML = messPassword
    }
    else {
        messInputs[index].innerHTML = ""
        if(index === inputs.length - 1) isInputValid= true
    }
}

inputs.forEach((input, index) => {
    input.addEventListener("keypress", function(event){
        if (event.key === "Enter") {
            event.preventDefault();
            validateInput(input, index)
        }
    })
})

const handleSubmit = (e) => {
    e.preventDefault();
    isInputValid = true;

    let outputString = "";

    inputs.forEach((input, index) => {
        validateInput(input, index);

        if (messInputs[index].innerHTML !== "") {
            isInputValid = false;
        } else {
            outputString += `${input.name}: ${input.value}\n`;
        }
    });

    if (isInputValid) {
        console.log(outputString);
    }
};
form.addEventListener('submit', handleSubmit);




















